package P3

import (
	"strconv"
	"workspace/P4"
)

var Name string
var Age int64
var Score float64

type person struct {
	name  string
	age   int64
	score float64
}

func Inputperson() person {
	Name = P4.Input("Enter your name:")
	Age, _ = strconv.ParseInt(P4.Input("Enter your age:"), 10, 64)
	Score, _ = strconv.ParseFloat(P4.Input("Enter your score:"), 64)
	return person{Name, Age, Score}
}
