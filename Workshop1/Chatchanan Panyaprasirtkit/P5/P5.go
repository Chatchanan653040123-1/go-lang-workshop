package P5

import (
	"workspace/SB"
)

func Fibonacci(num int64) string {
	array := []int{0, 1}
	for i := 2; i < int(num); i++ {
		array = append(array, array[i-1]+array[i-2])
	}
	stringBuilder := SB.StringBuilderInt(array)
	return stringBuilder
}
