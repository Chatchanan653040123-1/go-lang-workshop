package SB

import (
	"strconv"
	"strings"
)

func StringBuilderStr(array []string) string {
	var stringBuilder strings.Builder
	for i := 0; i < len(array); i++ {
		if i > 0 {
			stringBuilder.WriteString(" ")
		}
		stringBuilder.WriteString(array[i])
	}
	return stringBuilder.String()
}

func StringBuilderInt(array []int) string {
	var stringBuilder strings.Builder
	for i := 0; i < len(array); i++ {
		if i > 0 {
			stringBuilder.WriteString(" ")
		}
		stringBuilder.WriteString(strconv.Itoa(array[i]))
	}
	return stringBuilder.String()
}

func StringBuilderInt64(array []int64) string {
	var stringBuilder strings.Builder
	for i := 0; i < len(array); i++ {
		if i > 0 {
			stringBuilder.WriteString(" ")
		}
		stringBuilder.WriteString(strconv.FormatInt(array[i], 10))
	}
	return stringBuilder.String()
}

func StringBuilderFloat32(array []float32) string {
	var stringBuilder strings.Builder
	for i := 0; i < len(array); i++ {
		if i > 0 {
			stringBuilder.WriteString(" ")
		}
		stringBuilder.WriteString(strconv.FormatFloat(float64(array[i]), 'f', -1, 32))
	}
	return stringBuilder.String()
}

func StringBuilderFloat64(array []float64) string {
	var stringBuilder strings.Builder
	for i := 0; i < len(array); i++ {
		if i > 0 {
			stringBuilder.WriteString(" ")
		}
		stringBuilder.WriteString(strconv.FormatFloat(array[i], 'f', -1, 64))
	}
	return stringBuilder.String()
}
