package P4

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func Hello() string {
	return "Hello, " + Input("Enter your name:")
}
func Input(text string) string {
	fmt.Print(text)
	reader := bufio.NewReader(os.Stdin)
	output, _ := reader.ReadString('\n')
	output = strings.TrimSuffix(output, "\r\n")
	return output
}
