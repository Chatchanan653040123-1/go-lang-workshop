package main

import "fmt"

type student struct {
	studentName  string
	studentAge   string
	studentScore float64
}

func main() {
	s := student{
		studentName:  "John Doe",
		studentAge:   "20",
		studentScore: 80.5,
	}
	fmt.Println("Name :", s.studentName)
	fmt.Println("Age :", s.studentAge)
	fmt.Println("Score :", s.studentScore)

}
