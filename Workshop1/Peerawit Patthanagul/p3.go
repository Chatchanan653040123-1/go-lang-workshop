package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func getInput(promt string) float64{
	fmt.Printf("%v",promt)
	input, _ := reader.ReadString('\n')
	value, err := strconv.ParseFloat(strings.TrimSpace(input), 64)
	if err != nil{
		message, _ := fmt.Scanf("%v must be number only!",promt)
		panic(message)
	}
	return value
}

func Calculation(value1,value2 float64){
	fmt.Println("Addition result :",value1+value2) 
	fmt.Println("Subtraction result :",value1-value2)
	fmt.Println("Multiplication :",value1*value2)
	fmt.Println("Division result :",value1/value2)
}




func main() {
	value1 := getInput("Number 1 = ")
	value2 := getInput("Number 2 = ")
	if value2 ==0 {
		fmt.Println("Second Value cannot be 0")
	}else{
	Calculation(float64(value1),float64(value2))
	}
}
