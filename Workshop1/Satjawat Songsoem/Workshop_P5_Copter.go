package main

import "fmt"

var x, y, temp int

func Fibonacci(n int) {
	n1 := 1
	n2 := 1

	for i := 0; i < n; i++ {
		if i == 0 {
			fmt.Print(i, "\t")
		} else {
			fmt.Print(n1, "\t")
			temp = n1 + n2
			n1 = n2
			n2 = temp
		}

	}
}

func main() {
	Fibonacci(10)
}
