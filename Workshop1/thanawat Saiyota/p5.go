package main

import "fmt"

func calLCM(a []int) int {
    // หาค่าสูงสุด
    m := a[0]
    for _, num := range a {
        if num > m {
            m = num
        }
    }

    // กำหนดค่า LCM เป็นค่าสูงสุด
    lcm := m

    // วนลูปจนกว่าจะหาค่า LCM
    for {
        isLCM := true
        for _, num := range a {
            if lcm % num != 0 {
                isLCM = false
                break
            }
        }
        if isLCM {
            return lcm
        }
        lcm += m
    }
}

func main() {
	lst := []int{2, 4, 6, 8, 10}
	fmt.Printf("LCM is %d", calLCM(lst))
}
