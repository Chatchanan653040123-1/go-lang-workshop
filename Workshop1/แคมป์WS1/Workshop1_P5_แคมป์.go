package main

import "fmt"

func gcd(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func lcm(a, b int) int {
	return a * b / gcd(a, b)
}

func multiLcm(numbers []int) int {
	if len(numbers) < 2 {
		panic("input atleast two integers")
	}
	result := numbers[0]
	for i := 1; i < len(numbers); i++ {
		result = lcm(result, numbers[i])
	}
	return result
}

func main() {
	numbers := []int{10, 2, 6, 8, 4}
	result := multiLcm(numbers)
	fmt.Printf("The LCM of %v is %d\n", numbers, result)
}
