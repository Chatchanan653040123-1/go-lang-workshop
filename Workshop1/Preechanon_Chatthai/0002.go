package main

import "fmt"

var numbers int = 123

func checkPrimeNumbers(numbers int) bool {
	for i := 2; i < numbers; i++ {
		if numbers%i == 0 {
			return false
		}
	}

	return true
}

func main() {
	check := checkPrimeNumbers(numbers)
	if check {
		fmt.Printf("%d is prime", numbers)
	} else {
		fmt.Printf("%d is not prime", numbers)
	}
}
