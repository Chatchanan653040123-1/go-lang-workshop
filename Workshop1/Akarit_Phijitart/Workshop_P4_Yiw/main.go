package main

import (
	"fmt"
)

func main() {
	n := 12
	for i := 1; i <= n; i++ {
		for j := 1; j <= 12; j++ {
			fmt.Printf("%4d", i*j)
		}
		fmt.Println()
	}
}
