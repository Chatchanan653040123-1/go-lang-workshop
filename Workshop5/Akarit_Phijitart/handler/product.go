package handler

import (
	"fmt"
	"log"
	"strconv"

	"github.com/Akarit2001/workshop3/service"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

const appjson = "application/json"

// const contype = "content-type"

type productHandler struct {
	productService service.ProductService
}

func NewProductHandler(productService service.ProductService) productHandler {
	return productHandler{productService: productService}
}

func (h productHandler) GetProducts(c *fiber.Ctx) error {
	products, err := h.productService.GetProducts()
	if err != nil {
		return fiber.NewError(handleErr(err))
	}
	return c.JSON(products)
}

func (h productHandler) GetProduct(c *fiber.Ctx) error {
	pidStr := c.Params("pid")
	pid, err := strconv.Atoi(pidStr)
	if err != nil {
		// Return an HTTP 400 Bad Request error if pid is not a number
		return fiber.NewError(fiber.StatusBadRequest, "invalid product id")
	}
	product, err := h.productService.GetProduct(pid)
	if err != nil {
		return fiber.NewError(handleErr(err))
	}
	return c.JSON(product)
}

func (h productHandler) AddNewProduct(c *fiber.Ctx) error {

	// Check Content-Type header
	contentType := c.Get("Content-Type")
	if contentType != appjson {
		return fiber.NewError(fiber.StatusBadRequest, "Content-Type must be application/json")
	}
	request := service.ProductResquest{}
	err := c.BodyParser(&request)
	if err != nil {
		return err
	}
	v := validator.New()
	err = v.Struct(request)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, "data struct not matching")
	}

	response, err := h.productService.AddNewProduct(request)
	if err != nil {
		return fiber.NewError(handleErr(err))
	}
	return c.JSON(response)
}

func (h productHandler) UpdateProduct(c *fiber.Ctx) error {

	pidStr := c.Params("pid")
	pid, err := strconv.Atoi(pidStr)
	if err != nil {
		// Return an HTTP 400 Bad Request error if pid is not a number
		return c.Status(fiber.StatusBadRequest).SendString("invalid product id")
	}

	contentType := c.Get("Content-Type")
	if contentType != appjson {
		return fiber.NewError(fiber.StatusBadRequest, "Content-Type must be application/json")
	}
	request := service.ProductResquest{}
	err = c.BodyParser(&request)
	if err != nil {
		return err
	}
	v := validator.New()
	err = v.Struct(request)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, "data struct not matching")
	}

	response, err := h.productService.UpdateProduct(pid, request)
	if err != nil {
		return fiber.NewError(handleErr(err))
	}

	return c.JSON(response)
}

func (h productHandler) DeleteProduct(c *fiber.Ctx) error {
	pidStr := c.Params("pid")
	pid, err := strconv.Atoi(pidStr)
	if err != nil {
		// Return an HTTP 400 Bad Request error if pid is not a number
		return c.Status(fiber.StatusBadRequest).SendString("invalid product id")
	}
	err = h.productService.DeleteProduct(pid)
	if err != nil {
		return fiber.NewError(handleErr(err))
	}
	logMsg := fmt.Sprintf("Product id:%s deleted", pidStr)
	log.Println(logMsg)
	return c.SendStatus(fiber.StatusOK)
}
