package service

import (
	
	"github.com/stretchr/testify/mock"
	
)

type customerServiceMock struct {
	mock.Mock
}

func NewCustomerServiceMock() *customerServiceMock {
	return &customerServiceMock{}
}
