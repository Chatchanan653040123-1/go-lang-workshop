package service_test

import (
	"errors"
	"fmt"
	"server/service"
	"server/repository"

	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCustomers(t *testing.T) {
	type testCase struct {
		CustomerID   int
		CustomerName string
		PhoneNumber  string
		BirthDate    string
	}

	cases := []testCase{
		{CustomerID: 1, CustomerName: "Dollar", PhoneNumber: "100", BirthDate: "5/2/2023"},
		{CustomerID: 2, CustomerName: "Rallod", PhoneNumber: "101", BirthDate: "5/2/2023"},
		{CustomerID: 3, CustomerName: "BuBu", PhoneNumber: "102", BirthDate: "5/2/2023"},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("success : %s", c.CustomerName), func(t *testing.T) {
			custRepo := repository.NewCustomerRepositoryMock()
			expectedCustomers := []repository.Customer{{
				CustomerID:   c.CustomerID,
				CustomerName: c.CustomerName,
				PhoneNumber:  c.PhoneNumber,
				BirthDate:    c.BirthDate,
			}}
			custRepo.On("GetCustomers").Return(expectedCustomers, nil)

			custService := service.NewCustomerService(custRepo)

			getCustomers, err := custService.GetCustomers()
			assert.Nil(t, err)

			expectedResponses := []service.CustomerResponse{{
				CustomerID:   c.CustomerID,
				CustomerName: c.CustomerName,
				PhoneNumber:  c.PhoneNumber,
				BirthDate:    c.BirthDate,
			}}

			assert.Equal(t, expectedResponses, getCustomers)

			custRepo.AssertCalled(t, "GetCustomers")
		})

		t.Run(fmt.Sprintf("error : %s", c.CustomerName), func(t *testing.T) {
			custRepo := repository.NewCustomerRepositoryMock()
			expectedError := errors.New("unexpected error")
			custRepo.On("GetCustomers").Return([]repository.Customer{}, expectedError)
		
			custService := service.NewCustomerService(custRepo)
		
			getCustomers, err := custService.GetCustomers()
			assert.Nil(t, getCustomers)
			assert.Equal(t, expectedError.Error(), err.Error())
		
			custRepo.AssertCalled(t, "GetCustomers")
		})
	}
}
