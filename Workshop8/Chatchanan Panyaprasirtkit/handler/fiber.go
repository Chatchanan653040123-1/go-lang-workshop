package handler

import (
	"github.com/gofiber/fiber/v2"
)

func (h productHandler) GetProductsFiber(c *fiber.Ctx) error {
	products, err := h.prodSrv.GetProducts()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(err)
	}
	return c.Status(fiber.StatusOK).JSON(products)
}

// func (h productHandler) GetProductFiber(c *fiber.Ctx) error {
// 	productID, _ := strconv.Atoi(c.Params("ID"))
// 	product, err := h.prodSrv.GetProduct(productID)
// 	if err != nil {
// 		return c.Status(fiber.StatusInternalServerError).JSON(err)
// 	}
// 	return c.Status(fiber.StatusOK).JSON(product)
// }
// func (h productHandler) RemoveProductFiber(c *fiber.Ctx) error {
// 	ProductID, _ := strconv.Atoi(c.Params("ID"))
// 	err := h.prodSrv.DeleteProduct(ProductID)
// 	if err != nil {
// 		return c.Status(fiber.StatusInternalServerError).JSON(err)
// 	}
// 	return c.Status(fiber.StatusOK).JSON("Product successfully removed")
// }
// func (h productHandler) InsertProductFiber(c *fiber.Ctx) error {
// 	var Product service.ProductResponse
// 	err := c.BodyParser(&Product)
// 	if err != nil {
// 		return c.Status(fiber.StatusBadRequest).JSON(err)
// 	}
// 	insertedID, err := h.prodSrv.InsertProduct(Product)
// 	if err != nil {
// 		return c.Status(fiber.StatusInternalServerError).JSON(err)
// 	}
// 	return c.Status(fiber.StatusOK).JSON(insertedID)
// }
// func (h productHandler) UpdateProductFiber(c *fiber.Ctx) error {
// 	var Product service.ProductResponse
// 	err := c.BodyParser(&Product)
// 	if err != nil {
// 		return c.Status(fiber.StatusBadRequest).JSON(err)
// 	}
// 	updateID, err := h.prodSrv.UpdateProduct(Product)
// 	if err != nil {
// 		return c.Status(fiber.StatusInternalServerError).JSON(err)
// 	}
// 	return c.Status(fiber.StatusOK).JSON(fmt.Sprintf("Product successfully updated at ID:%d", updateID))
// }
