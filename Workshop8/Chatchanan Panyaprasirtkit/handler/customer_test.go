package handler_test

import (
	"testing"
	"workspace/repository"
	"workspace/service"

	"github.com/stretchr/testify/assert"
)

func TestGetAll(t *testing.T) {
	// success case
	t.Run("success", func(t *testing.T) {
		prodRepo := repository.NewProductRepositoryMock()
		expected := []repository.Product{
			{
				ID:           1,
				Product_name: "Product 1",
				Price:        1000,
				Date_created: "2020-01-01",
			},
		}
		prodRepo.On("GetAll").Return(expected, nil)
		prodService := service.NewProductService(prodRepo)
		actual, err := prodService.GetProducts()
		assert.Equal(t, expected, actual)
		assert.Nil(t, err)
		prodRepo.AssertExpectations(t)
	})
	// error case
	t.Run("error", func(t *testing.T) {
		prodRepo := repository.NewProductRepositoryMock()
		expected := []repository.Product{}
		prodRepo.On("GetAll").Return(expected, nil)
		prodService := service.NewProductService(prodRepo)
		actual, err := prodService.GetProducts()
		assert.Equal(t, expected, actual)
		assert.Nil(t, err)
		prodRepo.AssertExpectations(t)
	})
}
