package service_test

import (
	"testing"
	"workspace/repository"
	"workspace/service"

	"github.com/stretchr/testify/assert"
)

func TestGetAll(t *testing.T) {
	type testCase struct {
		ID          int
		ProductName string
		Price       float64
		DateCreated string
	}
	cases := []testCase{
		{1, "Product 1", 1000, "2020-01-01"},
		{2, "Product 2", 2000, "2020-01-02"},
		{3, "Product 3", 3000, "2020-01-03"},
	}
	for _, c := range cases {
		// success case
		t.Run(c.ProductName, func(t *testing.T) {
			prodRepo := repository.NewProductRepositoryMock()
			expected := []repository.Product{
				{
					ID:           c.ID,
					Product_name: c.ProductName,
					Price:        c.Price,
					Date_created: c.DateCreated,
				},
			}
			prodRepo.On("GetAll").Return(expected, nil)
			prodService := service.NewProductService(prodRepo)
			actual, err := prodService.GetProducts()
			assert.Equal(t, expected, actual)
			assert.Nil(t, err)
			prodRepo.AssertExpectations(t)
		})
		// error case
		t.Run(c.ProductName, func(t *testing.T) {
			prodRepo := repository.NewProductRepositoryMock()
			expected := []repository.Product{}
			prodRepo.On("GetAll").Return(expected, nil)
			prodService := service.NewProductService(prodRepo)
			actual, err := prodService.GetProducts()
			assert.Equal(t, expected, actual)
			assert.Nil(t, err)
			prodRepo.AssertExpectations(t)
		})
	}
}
