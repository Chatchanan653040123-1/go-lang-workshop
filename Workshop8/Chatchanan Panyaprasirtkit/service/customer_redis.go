package service

import (
	"context"
	"encoding/json"
	"fmt"
	"workspace/errs"
	"workspace/logs"
	"workspace/repository"

	//"time"
	"github.com/go-redis/redis/v8"
)

type productServiceRedis struct {
	prodRepo    repository.ProductRepository
	redisClient *redis.Client
}

func NewProductServiceRedis(prodRepo repository.ProductRepository, redisClient *redis.Client) ProductService {
	return productServiceRedis{prodRepo, redisClient}
}

func (s productServiceRedis) GetProducts() ([]ProductResponse, error) {

	key := "service::GetProducts"
	Products := []ProductResponse{}
	if ProductJson, err := s.redisClient.Get(context.Background(), key).Result(); err == nil {
		if json.Unmarshal([]byte(ProductJson), &Products); err == nil {
			fmt.Println("redis")
			return Products, nil
		}
	}

	//Response
	productGet, err := s.prodRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	proResponses := []ProductResponse{}
	for _, product := range productGet {
		proResponse := ProductResponse{
			ID:          product.ID,
			ProductName: product.Product_name,
			Price:       product.Price,
			DateCreated: product.Date_created,
		}
		proResponses = append(proResponses, proResponse)
	}

	fmt.Println("Database")
	return proResponses, nil
}

func (s productServiceRedis) GetProduct(int) (*ProductResponse, error) {
	return nil, nil
}

func (s productServiceRedis) InsertProduct(ProductResponse) (int, error) {
	return 0, nil
}

func (s productServiceRedis) DeleteProduct(int) error {
	return nil
}

func (s productServiceRedis) UpdateProduct(ProductResponse) (int, error) {
	return 0, nil
}
