package repository

import (
	"gorm.io/gorm"
)

type productRepositoryDB struct {
	db *gorm.DB
}

func NewProductRepositoryDB(db *gorm.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}

func (r productRepositoryDB) GetAll() ([]Product, error) {
	products := []Product{}
	err := r.db.Find(&products).Error
	if err != nil {
		return nil, err
	}
	return products, nil
}
func (r productRepositoryDB) GetById(id int) (*Product, error) {
	product := Product{}
	err := r.db.First(&product, id).Error
	if err != nil {
		return nil, err
	}
	return &product, nil
}
func (r productRepositoryDB) InsertProduct(product Product) (int, error) {
	err := r.db.Create(&product).Error
	if err != nil {
		return 0, err
	}
	return product.ID, nil
}
func (r productRepositoryDB) DeleteProduct(productID int) error {
	err := r.db.Delete(&Product{}, productID).Error
	if err != nil {
		return err
	}
	return nil
}
func (r productRepositoryDB) UpdateProduct(product Product) (int, error) {
	err := r.db.Save(&product).Error
	if err != nil {
		return 0, err
	}
	return product.ID, nil
}
