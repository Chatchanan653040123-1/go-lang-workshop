package repository

import (
	"github.com/stretchr/testify/mock"
)

type productRepositoryMock struct {
	mock.Mock
}

func NewProductRepositoryMock() *productRepositoryMock {
	return &productRepositoryMock{}
}
func (r productRepositoryMock) GetAll() ([]Product, error) {
	args := r.Called()
	return args.Get(0).([]Product), args.Error(1)
}
