package repository

type Product struct {
	ID           int     `gorm:"primaryKey"`
	Product_name string  `gorm:"column:product_name"`
	Price        float64 `gorm:"column:price"`
	Date_created string  `gorm:"column:date_created"`
}
type ProductRepository interface {
	GetAll() ([]Product, error)
	GetById(int) (*Product, error) //ที่ต้องมี Pointer เพราะอาจจะมีการ Return nil ถ้าเป็น struct จะ Return ไม่ได้
	InsertProduct(Product) (int, error)
	DeleteProduct(int) error
	UpdateProduct(Product) (int, error)
}
