package handler

import (
	"net/http"
	"workspace/service"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
)

type ProductHandlerRedis struct {
	prodService service.ProductService
	redisClient *redis.Client
}

func NewProductHandlerRedis(productsrv service.ProductService, redisClient *redis.Client) ProductHandlerRedis {
	return ProductHandlerRedis{productsrv, redisClient}
}

func (h ProductHandlerRedis) GetProducts(c *fiber.Ctx) error {

	products, err := h.prodService.GetProducts()
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{"Error": "StatusInternalServerError [Can't get AllProduct]"})
	}
	response := fiber.Map{
		"status":   "ok",
		"products": products,
	}

	return c.JSON(response)
}
