package service

import (
	"database/sql"
	"fmt"
	"strings"
	"workspace/errs"
	"workspace/logs"
	"workspace/repository"

	"go.uber.org/zap"
)

var prodStrings []string

type productService struct {
	prodRepo repository.ProductRepository
}

func NewProductService(prodRepo repository.ProductRepository) productService {
	return productService{prodRepo: prodRepo}
}

func (s productService) GetProducts() ([]ProductResponse, error) {
	products, err := s.prodRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	prodResponses := []ProductResponse{}

	for _, product := range products {
		prodString := fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s",
			product.ID, product.Product_name, product.Price, product.Date_created)
		prodStrings = append(prodStrings, prodString)
		prodResponse := ProductResponse{
			ID:          product.ID,
			ProductName: product.Product_name,
			Price:       product.Price,
			DateCreated: product.Date_created,
		}
		prodResponses = append(prodResponses, prodResponse)
	}
	responseString := strings.Join(prodStrings, "\n")
	logs.Info("Get data:" + responseString)
	return prodResponses, nil
}
func (s productService) GetProduct(id int) (*ProductResponse, error) {
	product, err := s.prodRepo.GetById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("product not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	prodResponse := ProductResponse{
		ID:          product.ID,
		ProductName: product.Product_name,
		Price:       product.Price,
		DateCreated: product.Date_created,
	}
	logs.Info("User get data from ID:", zap.Int("id", id), zap.String("product", prodResponse.ProductName), zap.Float64("price", prodResponse.Price), zap.String("Date", prodResponse.DateCreated))
	return &prodResponse, nil
}
func (s productService) DeleteProduct(id int) error {
	err := s.prodRepo.DeleteProduct(id)
	if err != nil {
		logs.Error(err)
		return errs.NewNotFoundError("Product Not Found")
	}

	logs.Info("Delete Product: ", zap.Int("id", id))
	return nil
}

func (s productService) InsertProduct(ProductResponse ProductResponse) (int, error) {
	Product := repository.Product{
		ID:           ProductResponse.ID,
		Product_name: ProductResponse.ProductName,
		Price:        ProductResponse.Price,
		Date_created: ProductResponse.DateCreated,
	}

	ProductID, err := s.prodRepo.InsertProduct(Product)
	if err != nil {
		logs.Error(err)
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Insert Product  : ", zap.Int("id", ProductID))
	return ProductID, nil

}

func (s productService) UpdateProduct(ProductResponse ProductResponse) (int, error) {
	Product := repository.Product{
		ID:           ProductResponse.ID,
		Product_name: ProductResponse.ProductName,
		Price:        ProductResponse.Price,
		Date_created: ProductResponse.DateCreated,
	}

	ProductID, err := s.prodRepo.UpdateProduct(Product)
	if err != nil {
		logs.Error(err)
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Update Product  : ", zap.Int("id", ProductID))
	return ProductID, nil
}
