package repository

type Customer struct {
	CustomerID   string    `db:"Customer_id"`
	CustomerName string `db:"Customer_name"`
	PhoneNumber  string `db:"Phone_number"`
	BirthDate  string `db:"Birth_date"`
}

type CustomerRepository interface {
	GetAll() ([]Customer, error)
	GetById(int) (*Customer, error)
	InsertCustomer(Customer) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(Customer) (int, error)
}
