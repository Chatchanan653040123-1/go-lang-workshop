package service

import (
	"database/sql"
	"fmt"
	"strings"
	"workspace/errs"
	"workspace/logs"
	"workspace/repository"
)

var prodStrings []string

type productService struct {
	prodRepo repository.ProductRepository
}

func NewProductService(prodRepo repository.ProductRepository) productService {
	return productService{prodRepo: prodRepo}
}

func (s productService) GetProducts() ([]ProductResponse, error) {
	products, err := s.prodRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	prodResponses := []ProductResponse{}

	for _, product := range products {
		prodString := fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s",
			product.ID, product.Product_name, product.Price, product.Date_created)
		prodStrings = append(prodStrings, prodString)
		prodResponse := ProductResponse{
			ID:          product.ID,
			ProductName: product.Product_name,
			Price:       product.Price,
			DateCreated: product.Date_created,
		}
		prodResponses = append(prodResponses, prodResponse)
	}
	responseString := strings.Join(prodStrings, "\n")
	logs.Info("Get datas ===>" + responseString)
	return prodResponses, nil
}
func (s productService) GetProduct(id int) (*ProductResponse, error) {
	product, err := s.prodRepo.GetById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("product not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	prodResponse := ProductResponse{
		ID:          product.ID,
		ProductName: product.Product_name,
		Price:       product.Price,
		DateCreated: product.Date_created,
	}
	logs.Info("Get data ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
	return &prodResponse, nil
}
func (s productService) DeleteProduct(id int) error {
	product, _ := s.prodRepo.GetById(id)
	err := s.prodRepo.DeleteProduct(id)
	if err != nil {
		logs.Error(err)
		return errs.NewNotFoundError("Product Not Found")
	}

	logs.Info("Delete data ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
	return nil
}

func (s productService) InsertProduct(ProductResponse ProductResponse) (int, error) {
	Product := repository.Product{
		ID:           ProductResponse.ID,
		Product_name: ProductResponse.ProductName,
		Price:        ProductResponse.Price,
		Date_created: ProductResponse.DateCreated,
	}

	ProductID, err := s.prodRepo.InsertProduct(Product)
	product, _ := s.prodRepo.GetById(ProductID)
	if err != nil {
		logs.Error(err)
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Get data ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
	return ProductID, nil

}

func (s productService) UpdateProduct(ProductResponse ProductResponse) (int, error) {
	Product := repository.Product{
		ID:           ProductResponse.ID,
		Product_name: ProductResponse.ProductName,
		Price:        ProductResponse.Price,
		Date_created: ProductResponse.DateCreated,
	}

	ProductID, err := s.prodRepo.UpdateProduct(Product)
	product, _ := s.prodRepo.GetById(ProductID)

	if err != nil {
		logs.Error(err)
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Update Product ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
	return ProductID, nil
}
