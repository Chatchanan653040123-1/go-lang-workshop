package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"
	"workspace/logs"

	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

type ProductRepositoryRedis struct {
	db          *gorm.DB
	redisClient *redis.Client
}

func NewProductRepositoryRedis(db *gorm.DB, redisClient *redis.Client) ProductRepository {
	db.AutoMigrate(&Product{})
	mockData(db)
	return ProductRepositoryRedis{db, redisClient}
}
func mockData(db *gorm.DB) error {
	var count int64
	db.Model(&Product{}).Count(&count)
	if count > 0 {
		return nil
	}
	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)
	products := []Product{}
	for i := 0; i < 5000; i++ {
		products = append(products, Product{
			Product_name: "Product " + string(i),
			Price:        float64(random.Intn(1000000)),
			Date_created: time.Now().Format("2006-01-2 15:04:05"),
		})
	}
	return db.Create(&products).Error
}
func (r ProductRepositoryRedis) GetAll() (products []Product, err error) {

	//redis get
	key := "repository::GetAll"
	productsJson, err := r.redisClient.Get(context.Background(), key).Result()
	fmt.Println(productsJson)
	if err != nil {
		logs.Error(err)
		err = json.Unmarshal([]byte(productsJson), &products)
		if err != nil {
			logs.Error(err)
			fmt.Println("redis")
			return products, nil
		}
	}

	exec := r.db.Find(&products)
	if exec.Error != nil {
		return nil, exec.Error
	}
	fmt.Println("database")
	return products, nil
}

func (r ProductRepositoryRedis) GetById(int) (*Product, error) {
	return nil, nil
}

func (r ProductRepositoryRedis) InsertProduct(Product) (int, error) {
	return 0, nil
}

func (r ProductRepositoryRedis) DeleteProduct(int) error {
	return nil
}

func (r ProductRepositoryRedis) UpdateProduct(Product) (int, error) {
	return 0, nil
}
