package controllers

import (
	"producer/commands"
	"producer/services"

	"github.com/gofiber/fiber/v2"
)

type ProductController interface {
	InsertProduct(c *fiber.Ctx) error
	RemoveProduct(c *fiber.Ctx) error
	UpdateProduct(c *fiber.Ctx) error
}

type productController struct {
	prodService services.ProductService
}

func NewProductController(prodService services.ProductService) productController {
	return productController{prodService}
}
func (obj productController) InsertProduct(c *fiber.Ctx) error {
	command := commands.InsertProductCommand{}
	if err := c.BodyParser(&command); err != nil {
		return err
	}
	if id, err := obj.prodService.InsertProduct(command); err != nil {
		return err
	} else {
		return c.JSON(fiber.Map{"id": id})
	}
}
func (obj productController) RemoveProduct(c *fiber.Ctx) error {
	command := commands.DeleteProductCommand{}
	if err := c.BodyParser(&command); err != nil {
		return err
	}
	if id, err := obj.prodService.RemoveProduct(command); err != nil {
		return err
	} else {
		return c.JSON(fiber.Map{"id": id})
	}
}
func (obj productController) UpdateProduct(c *fiber.Ctx) error {
	command := commands.UpdateProductCommand{}
	if err := c.BodyParser(&command); err != nil {
		return err
	}
	if id, err := obj.prodService.UpdateProduct(command); err != nil {
		return err
	} else {
		return c.JSON(fiber.Map{"id": id})
	}
}
