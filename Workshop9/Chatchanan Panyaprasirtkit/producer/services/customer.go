package services

import (
	"errors"
	"events"
	"log"
	"producer/commands"
	"strconv"

	"github.com/google/uuid"
)

type ProductService interface {
	InsertProduct(command commands.InsertProductCommand) (id int, err error)
	DeleteProduct(command commands.DeleteProductCommand) (id int, err error)
	UpdateProduct(command commands.UpdateProductCommand) (id int, err error)
}

type productService struct {
	eventProducer EventProducer
}

func NewProductService(eventProducer EventProducer) ProductService {
	return productService{eventProducer}
}

func (obj productService) InsertProduct(command commands.InsertProductCommand) (id int, err error) {
	integer, err := strconv.Atoi(uuid.NewString())
	if err != nil {
		return 0, errors.New("bad request")
	}
	event := events.InsertProduct{
		ID:           integer,
		ProductName:  command.ProductName,
		Price:        command.Price,
		Date_created: command.Date_created,
	}

	log.Printf("%#v", event)
	return event.ID, obj.eventProducer.Produce(event)
}
func (obj productService) DeleteProduct(command commands.DeleteProductCommand) (id int, err error) {
	integer, err := strconv.Atoi(command.ID)
	if err != nil {
		return 0, errors.New("bad request")
	}
	event := events.DeleteProduct{
		ID: strconv.Itoa(integer),
	}

	log.Printf("%#v", event)
	return integer, obj.eventProducer.Produce(event)
}
func (obj productService) UpdateProduct(command commands.UpdateProductCommand) (id int, err error) {
	integer, err := strconv.Atoi(command.ID)
	if err != nil {
		return 0, errors.New("bad request")
	}
	event := events.UpdateProduct{
		ID:           integer,
		ProductName:  command.ProductName,
		Price:        command.Price,
		Date_created: command.Date_created,
	}

	log.Printf("%#v", event)
	return event.ID, obj.eventProducer.Produce(event)
}
