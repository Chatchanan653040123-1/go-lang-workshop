package main

import (
	"producer/controllers"
	"producer/services"
	"strings"

	"github.com/Shopify/sarama"
	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {
	producer, err := sarama.NewSyncProducer(viper.GetStringSlice("kafka.servers"), nil)
	if err != nil {
		panic(err)
	}
	defer producer.Close()

	eventProducer := services.NewEventProducer(producer)
	prodService := services.NewProductService(eventProducer)
	prodController := controllers.NewProductController(prodService)

	app := fiber.New()

	app.Post("/Insert", prodController.InsertProduct)
	app.Post("/Update", prodController.UpdateProduct)
	app.Post("/Delete", prodController.DeleteProduct)

	app.Listen(":8000")
}
