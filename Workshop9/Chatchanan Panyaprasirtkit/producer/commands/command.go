package commands

type InsertProductCommand struct {
	ID           int
	ProductName  string
	Price        float64
	Date_created string
}

type DeleteProductCommand struct {
	ID int
}

type UpdateProductCommand struct {
	ID           int
	ProductName  string
	Price        float64
	Date_created string
}
