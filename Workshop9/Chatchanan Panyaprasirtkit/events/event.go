package events

import "reflect"

var Topics = []string{
	reflect.TypeOf(GetProducts{}).Name(),
	reflect.TypeOf(GetProduct{}).Name(),
	reflect.TypeOf(InsertProduct{}).Name(),
	reflect.TypeOf(DeleteProduct{}).Name(),
	reflect.TypeOf(UpdateProduct{}).Name(),
}

type Event interface {
}

type GetProducts struct {
	ID int
}

type GetProduct struct {
	ID int
}

type InsertProduct struct {
	ID           int
	ProductName  string
	Price        float64
	Date_created string
}

type DeleteProduct struct {
	ID string
}

type UpdateProduct struct {
	ID           int
	ProductName  string
	Price        float64
	Date_created string
}
