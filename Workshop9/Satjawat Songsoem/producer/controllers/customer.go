package controllers

import (
	"producer/commands"
	"producer/services"

	"github.com/gofiber/fiber/v2"
)

type CustomerController interface {
	//GetCustomers(c *fiber.Ctx) error
	//GetCustomer(c *fiber.Ctx) error
	InsertCustomer(c *fiber.Ctx) error
	RemoveCustomer(c *fiber.Ctx) error
	UpdateCustomer(c *fiber.Ctx) error
}

type customerController struct {
	custService services.CustomerService
}

func NewCustomerController(custService services.CustomerService) customerController {
	return customerController{custService}
}

/*
func(obj customerController) GetCustomers(c *fiber.Ctx) error {
	command := commands.GetCustomersCommand{}
	err := c.BodyParser(&command)
	if err != nil {
		return err
	}

	err = obj.custService.GetCustomers(command)
	if err != nil {
		return err
	}

	c.Status(fiber.StatusOK)
	return c.JSON(fiber.Map{
		"Message": "Get ",
	})
}*/

func (obj customerController) InsertCustomer(c *fiber.Ctx) error {
	command := commands.InsertCustomerCommand{}
	err := c.BodyParser(&command)
	if err != nil {
		return err
	}

	id, err := obj.custService.InsertCustomer(command)
	if err != nil {
		return err
	}

	c.Status(fiber.StatusOK)
	return c.JSON(fiber.Map{
		"Message": "Insert Success",
		"ID":      id,
	})
}

func (obj customerController) UpdateCustomer(c *fiber.Ctx) error {
	command := commands.UpdateCustomerCommand{}
	err := c.BodyParser(&command)
	if err != nil {
		return err
	}

	id, err := obj.custService.UpdateCustomer(command)
	if err != nil {
		return err
	}

	c.Status(fiber.StatusOK)
	return c.JSON(fiber.Map{
		"Message": "Update Success",
		"ID":      id,
	})
}

func (obj customerController) RemoveCustomer(c *fiber.Ctx) error {
	command := commands.RemoveCustomerCommand{}
	err := c.BodyParser(&command)
	if err != nil {
		return err
	}

	id, err := obj.custService.RemoveCustomer(command)
	if err != nil {
		return err
	}

	c.Status(fiber.StatusOK)
	return c.JSON(fiber.Map{
		"Message": "Remove Success",
		"ID":      id,
	})
}
