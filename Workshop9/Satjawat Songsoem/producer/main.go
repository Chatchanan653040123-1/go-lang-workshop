package main

import (
	"producer/controllers"
	"producer/services"
	"strings"

	"github.com/Shopify/sarama"
	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {
	producer, err := sarama.NewSyncProducer(viper.GetStringSlice("kafka.servers"), nil)
	if err != nil {
		panic(err)
	}
	defer producer.Close()

	eventProducer := services.NewEventProducer(producer)
	custService := services.NewCustomerService(eventProducer)
	custController := controllers.NewCustomerController(custService)

	app := fiber.New()

	//app.Post("/GetCustomers", custController.GetCustomers)
	//app.Post("/GetCustomer", custController.GetCustomer)
	app.Post("/InsertCustomer", custController.InsertCustomer)
	app.Post("/UpdateCustomer", custController.UpdateCustomer)
	app.Post("/RemoveCustomer", custController.RemoveCustomer)

	app.Listen(":8000")
}