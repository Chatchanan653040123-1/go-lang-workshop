package services

import (
	"errors"
	"events"
	"log"
	"producer/commands"

	"github.com/google/uuid"
)


type CustomerService interface {
	//GetCustomers(command commands.GetCustomersCommand) error
	//GetCustomer(command commands.GetCustomerCommand)  error
	InsertCustomer(command commands.InsertCustomerCommand) (id string, err error)
	RemoveCustomer(command commands.RemoveCustomerCommand) (id string, err error)
	UpdateCustomer(command commands.UpdateCustomerCommand) (id string, err error)
}

type customerService struct {
	eventProducer EventProducer
}

func NewCustomerService(eventProducer EventProducer) CustomerService {
	return customerService{eventProducer}
}
/*
func (obj customerService) GetCustomers(command commands.GetCustomersCommand) error {
	
	if command.CustomerID != 0 {
		return errors.New("bad request")
	}

	event := events.GetCustomers{
		CustomerID: command.CustomerID,
	}
	
	return obj.eventProducer.Produce(event)
}

func (obj customerService) GetCustomer(command commands.GetCustomerCommand) error {
	
	if command.CustomerID == 0 {
		return errors.New("bad request")
	}

	event := events.GetCustomer{
		CustomerID: command.CustomerID,
	}

	return obj.eventProducer.Produce(event)
}
*/

func (obj customerService) InsertCustomer(command commands.InsertCustomerCommand) (id string, err error) {

	if command.CustomerName == "" || command.PhoneNumber == "" || command.BirthDate == "" {
		return "0", errors.New("bad request")
	}

	event := events.InsertCustomer{
		CustomerID:   uuid.NewString(),
		CustomerName: command.CustomerName,
		PhoneNumber:  command.PhoneNumber,
		BirthDate:    command.BirthDate,
	}

	log.Printf("%#v", event)
	return event.CustomerID, obj.eventProducer.Produce(event)
}

func (obj customerService) UpdateCustomer(command commands.UpdateCustomerCommand) (id string, err error) {

	if len(command.CustomerID) != 36 || command.CustomerName == "" || command.PhoneNumber == "" || command.BirthDate == "" {
		return "0", errors.New("bad request")
	}

	event := events.UpdateCustomer{
		CustomerID:   command.CustomerID,
		CustomerName: command.CustomerName,
		PhoneNumber:  command.PhoneNumber,
		BirthDate:    command.BirthDate,
	}

	log.Printf("%#v", event)
	return event.CustomerID, obj.eventProducer.Produce(event)
}

func (obj customerService) RemoveCustomer(command commands.RemoveCustomerCommand) (id string, err error) {
	if len(command.CustomerID) != 36 {
		return "0", errors.New("bad request")
	}

	event := events.RemoveCustomer{
		CustomerID: command.CustomerID,
	}

	log.Printf("%#v", event)
	return event.CustomerID, obj.eventProducer.Produce(event)
}
