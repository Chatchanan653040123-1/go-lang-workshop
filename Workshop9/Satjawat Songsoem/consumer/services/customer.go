package services

import (
	"consumer/repositories"
	"encoding/json"
	"events"
	"log"
	"reflect"
)

type EventHandler interface {
	Handle(topic string, eventBytes []byte)
}

type customerEventHandler struct {
	custRepo repositories.CustomerRepository
}

func NewCustomerEventHandler(custRepo repositories.CustomerRepository) EventHandler {
	return &customerEventHandler{custRepo}
}

func (obj *customerEventHandler) Handle(topic string, eventBytes []byte) {
	switch topic {
	/*
	case reflect.TypeOf(events.GetCustomers{}).Name():
		event := &events.GetCustomer{}
		err := json.Unmarshal(eventBytes, event)
		if err != nil {
			log.Println(err)

		}

		if event.CustomerID == 0{
			customers, err := obj.custRepo.GetCustomers()
			if err != nil {
			log.Println(err)
			return
			}
			log.Printf("[%v] %#v", topic, customers)
		}
		

	case reflect.TypeOf(events.GetCustomer{}).Name():
		event := &events.GetCustomer{}
		err := json.Unmarshal(eventBytes, event)
		if err != nil {
			log.Println(err)

		}
		customer, err := obj.custRepo.GetCustomer(event.CustomerID)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("[%v] %#v", topic, customer)
		*/

	case reflect.TypeOf(events.InsertCustomer{}).Name():
		event := &events.InsertCustomer{}
		err := json.Unmarshal(eventBytes, event)
		if err != nil {
			log.Println(err)
			return
		}
		customer := repositories.Customer{
			CustomerID:   event.CustomerID,
			CustomerName: event.CustomerName,
			PhoneNumber:   event.PhoneNumber,
			BirthDate:       event.BirthDate,
		}

		insertErr := obj.custRepo.InsertCustomer(customer)
		if insertErr  != nil {
			log.Println(insertErr)
			return
		}

		log.Printf("[%v] Inserted %v", topic, event)

	case reflect.TypeOf(events.RemoveCustomer{}).Name():
		event := &events.RemoveCustomer{}
		err := json.Unmarshal(eventBytes, event)
		if err != nil {
			log.Println(err)
			return
		}
		removeErr := obj.custRepo.RemoveCustomer(event.CustomerID)
		if removeErr != nil {
			log.Println(removeErr)
			return
		}

		log.Printf("[%v] Removed %#v", topic, event)

	case reflect.TypeOf(events.UpdateCustomer{}).Name():
		event := &events.UpdateCustomer{}
		err := json.Unmarshal(eventBytes, event)
		if err != nil {
			log.Println(err)
			return
		}

		customer := repositories.Customer{
			CustomerID:   event.CustomerID,
			CustomerName: event.CustomerName,
			PhoneNumber:   event.PhoneNumber,
			BirthDate:       event.BirthDate,
		}

		updateErr := obj.custRepo.UpdateCustomer(customer)
		if updateErr != nil {
			log.Println(updateErr)
			return
		}

		log.Printf("[%v] Updated %#v", topic, event)

	default:
		log.Printf("Unknown topic: %s", topic)
		return
	}
}

	
