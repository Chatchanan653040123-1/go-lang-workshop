package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Product struct {
	ID          int       `json:"id"`
	ProductName string    `json:"productName"`
	Price       float64   `json:"price"`
	DateCreated time.Time `json:"dateCreated"`
}

const (
	username = "root"
	password = ""
	hostname = "127.0.0.1:3306"
	dbname   = "workshop2"
)

var ProductList []Product
var requestedValue Product
var db *sql.DB
var err error

func initDatabase(dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
}
func init() {
	db, err = dbConnection()
	if err != nil {
		log.Printf("%s when try to connect to database!", err)
	}
	rows, err := db.Query("SELECT id, product_name, price, date_created FROM product")
	if err != nil {
		log.Printf("Error querying database: %s", err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var p Product
		var dateCreatedStr string
		err := rows.Scan(&p.ID, &p.ProductName, &p.Price, &dateCreatedStr)
		if err != nil {
			log.Printf("Error scanning rows: %s", err)
			return
		}
		p.DateCreated, err = time.Parse("2006-01-02 15:04:05", dateCreatedStr)
		if err != nil {
			log.Printf("Error parsing date created: %s", err)
			return
		}
		ProductList = append(ProductList, p)
	}

}
func dbConnection() (*sql.DB, error) {
	db, err := sql.Open("mysql", initDatabase(dbname))
	db.SetMaxOpenConns(1)
	db.SetMaxIdleConns(1)
	db.SetConnMaxLifetime(time.Minute * 5)
	if err != nil {
		return nil, err
	}
	_, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	log.Printf("Connected to DB %s successfully\n", dbname)
	return db, nil
}

func insertData(db *sql.DB) error {
	query := `INSERT INTO product (product_name, price) VALUES (?, ?)`
	ctx, cancelfunc := context.WithTimeout(context.Background(), time.Second*5)
	defer cancelfunc()
	_, err := db.ExecContext(ctx, query, requestedValue.ProductName, requestedValue.Price)
	if err != nil {
		return err
	}
	log.Print("Insert values successfuly")
	return nil
}

func productHandler(w http.ResponseWriter, r *http.Request) {
	requestedValue.DateCreated = time.Now()
	switch r.Method {
	case http.MethodGet:
		id := getProductID(r)
		if id != -1 {
			for _, p := range ProductList {
				if p.ID == id {
					productJSON, err := json.Marshal(p)
					if err != nil {
						w.WriteHeader(http.StatusInternalServerError)
						return
					}
					w.Header().Set("Content-Type", "application/json")
					w.Write(productJSON)
					return
				}
			}
			w.WriteHeader(http.StatusNotFound)
			return
		} else {
			productJSON, err := json.Marshal(ProductList)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.Write(productJSON)
		}

	case http.MethodPost:
		err := json.NewDecoder(r.Body).Decode(&requestedValue)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		requestedValue.ID = getNextID()
		ProductList = append(ProductList, requestedValue)
		log.Println(insertData(db))
		w.WriteHeader(http.StatusCreated)

	case http.MethodPut:
		err := json.NewDecoder(r.Body).Decode(&requestedValue)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		id := getProductID(r)
		if id == -1 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		stmt, err := db.Prepare("UPDATE product SET product_name=?, price=?, date_created=? WHERE id=?")
		if err != nil {
			http.Error(w, "Error preparing SQL statement", http.StatusInternalServerError)
			return
		}
		defer stmt.Close()

		res, err := stmt.Exec(requestedValue.ProductName, requestedValue.Price, requestedValue.DateCreated, id)
		if err != nil {
			http.Error(w, "Error executing SQL statement", http.StatusInternalServerError)
			return
		}

		affected, err := res.RowsAffected()
		if err != nil {
			http.Error(w, "Error checking rows affected", http.StatusInternalServerError)
			return
		}
		if affected == 0 {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		for i, p := range ProductList {
			if p.ID == id {
				ProductList[i] = requestedValue
				break
			}
		}
		w.WriteHeader(http.StatusOK)

	case http.MethodDelete:
		id := getProductID(r)
		if id == -1 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		result, err := db.Exec("DELETE FROM product WHERE id=?", id)
		if err != nil {
			log.Printf("Error deleting product with id %d: %s", id, err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		rowsAffected, err := result.RowsAffected()
		if err != nil {
			log.Printf("Error getting number of rows affected: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if rowsAffected == 0 {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func getNextID() int {
	highestID := -1
	for _, product := range ProductList {
		if highestID < product.ID {
			highestID = product.ID
		}
	}
	return highestID + 1
}

func getProductID(r *http.Request) int {
	idStr := strings.TrimPrefix(r.URL.Path, "/product/")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return -1
	}
	return id
}
func loggingMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
		if r.Method == http.MethodGet && getProductID(r) == -1 {
			log.Printf("%s_%s_values:%v", r.URL.Path, r.Method, ProductList)
		} else {
			log.Printf("%s_%s_Request values:%v", r.URL.Path, r.Method, requestedValue)
		}
	}
}

func corsMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		if r.Method == "OPTIONS" {
			return
		}
		next.ServeHTTP(w, r)
	}
}

func main() {
	http.Handle("/product/", corsMiddleware(loggingMiddleware(productHandler)))
	http.ListenAndServe(":5000", nil)
	db.Close()
}
