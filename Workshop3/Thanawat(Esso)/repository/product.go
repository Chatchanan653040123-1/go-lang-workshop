package repository

type Product struct {
	ProductId     int    `db:"id"`
	Name          string `db:"product_name"`
	Price         string `db:"price"`
	ProductDetail string `db:"product_detail"`
	DateCreate    string `db:"date_created"`
}

type ProductRepository interface {
	GetAll() ([]Product, error)
	GetById(int) (*Product, error)
	Insert(Product) error
	Update(int, Product) error
	Delete(int) error
}
