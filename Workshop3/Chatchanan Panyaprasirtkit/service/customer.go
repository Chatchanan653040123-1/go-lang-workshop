package service

type ProductResponse struct {
	ID          int     `json:"id"`
	ProductName string  `json:"productName"`
	Price       float64 `json:"price"`
	DateCreated string  `json:"dateCreated"`
}

type ProductService interface {
	GetProducts() ([]ProductResponse, error)
	GetProduct(int) (*ProductResponse, error)
	InsertProduct(ProductResponse) (int, error)
	DeleteProduct(int) error
	UpdateProduct(ProductResponse) (int, error)
}
