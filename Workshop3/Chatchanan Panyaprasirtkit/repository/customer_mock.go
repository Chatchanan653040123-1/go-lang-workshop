package repository

import (
	"errors"
	"time"
)

type productRepositoryMock struct {
	products []Product
}

func NewProductRepositoryMock() productRepositoryMock {
	products := []Product{
		{
			ID:           0,
			Product_name: "KFC",
			Price:        299.0,
			Date_created: time.Now().Format("2006-01-2 15:04:05"),
		},
		{
			ID:           1,
			Product_name: "Pizza",
			Price:        159.0,
			Date_created: time.Now().Format("2006-01-2 15:04:05"),
		},
	}
	return productRepositoryMock{products: products}
}
func (r productRepositoryMock) GetAll() ([]Product, error) {
	return r.products, nil
}
func (r productRepositoryMock) GetById(id int) (*Product, error) {
	for _, product := range r.products {
		if product.ID == id {
			return &product, nil
		}
	}
	return nil, errors.New("product not found")
}
