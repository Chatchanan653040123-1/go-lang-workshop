package repository

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
)

type productRepositoryDB struct {
	db *sqlx.DB
}

func NewProductRepositoryDB(db *sqlx.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}
func (r productRepositoryDB) GetAll() ([]Product, error) {
	products := []Product{}
	query := "SELECT id, product_name, price, date_created FROM product"
	err := r.db.Select(&products, query)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (r productRepositoryDB) GetById(id int) (*Product, error) {
	product := Product{}
	query := "select id, product_name, price, date_created FROM product where id=?"
	err := r.db.Get(&product, query, id)
	if err != nil {
		return nil, err
	}
	return &product, nil
}
func (r productRepositoryDB) InsertProduct(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "INSERT INTO product(id, product_name, price, date_created) VALUES (?, ?, ?,?)"
	result, err := r.db.ExecContext(ctx, query, product.ID, product.Product_name, product.Price, product.Date_created)
	if err != nil {
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(lastInsertID), nil
}

func (r productRepositoryDB) DeleteProduct(productID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "DELETE FROM product WHERE id = ?"
	_, err := r.db.ExecContext(ctx, query, productID)
	if err != nil {
		return err
	}
	return nil
}

func (r productRepositoryDB) UpdateProduct(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "UPDATE product SET product_name = ?, price = ?, date_created = ? WHERE id = ?"
	result, err := r.db.ExecContext(ctx, query, product.Product_name, product.Price, product.Date_created, product.ID)
	if err != nil {
		return 0, err
	}
	updateID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(updateID), nil
}
