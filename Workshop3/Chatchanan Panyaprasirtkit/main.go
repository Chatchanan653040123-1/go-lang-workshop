package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"
	"workspace/handler"
	"workspace/logs"
	"workspace/repository"
	"workspace/service"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

func main() {
	initTimeZone()
	initConfig()
	db := initDatabase()
	ProductRepositoryDB := repository.NewProductRepositoryDB(db)
	ProductService := service.NewProductService(ProductRepositoryDB)
	ProductHandler := handler.NewProductHandler(ProductService)

	router := mux.NewRouter()
	router.HandleFunc("/Products", ProductHandler.GetProducts).Methods(http.MethodGet)
	router.HandleFunc("/Products/{ID:[0-9]+}", ProductHandler.GetProduct).Methods(http.MethodGet)
	router.HandleFunc("/Products", ProductHandler.InsertProduct).Methods(http.MethodPost)
	router.HandleFunc("/Products/{ID:[0-9]+}", ProductHandler.RemoveProduct).Methods(http.MethodDelete)
	router.HandleFunc("/Products/{ID:[0-9]+}", ProductHandler.UpdateProduct).Methods(http.MethodPut)
	logs.Info("service started at port " + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
}
func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}
func initDatabase() *sqlx.DB {
	db, err := sqlx.Open(viper.GetString("db.driver"), fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	))
	if err != nil {
		logs.Error(err)
		return nil
	}
	logs.Info(fmt.Sprintf("%v is connected successfuly", db))
	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	return db
}
