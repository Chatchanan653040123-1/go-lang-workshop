package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"workspace/service"

	"github.com/gorilla/mux"
)

type productHandler struct {
	prodSrv service.ProductService
}

func NewProductHandler(prodSrv service.ProductService) productHandler {
	return productHandler{prodSrv: prodSrv}
}
func (h productHandler) GetProducts(w http.ResponseWriter, r *http.Request) {
	products, err := h.prodSrv.GetProducts()
	if err != nil {
		handleError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(products)
}
func (h productHandler) GetProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["ID"])
	product, err := h.prodSrv.GetProduct(productID)
	if err != nil {
		handleError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(product)

}
func (h productHandler) RemoveProduct(w http.ResponseWriter, r *http.Request) {
	ProductID, _ := strconv.Atoi(mux.Vars(r)["ID"])
	err := h.prodSrv.DeleteProduct(ProductID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Product successfully removed"))
}

func (h productHandler) InsertProduct(w http.ResponseWriter, r *http.Request) {
	var Product service.ProductResponse
	err := json.NewDecoder(r.Body).Decode(&Product)
	if err != nil {
		handleError(w, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	insertedID, err := h.prodSrv.InsertProduct(Product)

	if err != nil {
		handleError(w, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf(`{"Productid:%d"}`, insertedID)))
}

func (h productHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	var Product service.ProductResponse
	err := json.NewDecoder(r.Body).Decode(&Product)
	if err != nil {
		handleError(w, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	updateID, err := h.prodSrv.UpdateProduct(Product)

	if err != nil {
		handleError(w, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf(`{"Productid:%d"}`, updateID)))
}
