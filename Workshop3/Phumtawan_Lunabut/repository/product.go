package repository

type Product struct {
	ID				int		`db:"id"`
	Product_Name	string	`db:"product_name"`
	Price			float64	`db:"price"`
	Product_Detail	string	`db:"product_detail"`
	Date_Created	string	`db:"date_created"`
}

type ProductRepository interface {
	Create(Product)(*Product, error)
	Update(int, Product) (*Product,error)
	GetByID(int) (*Product, error)
	GetAllProduct() ([]Product, error)
	Delete(int) (error)
}