package services

import (
	context "context"
	"server/logs"
	"server/repository"

	"go.uber.org/zap"
)

var prodStrings []string

type productServer struct {
	prodRepo repository.ProductRepository
}

func NewProductService(prodRepo repository.ProductRepository) ProductServer {
	return productServer{prodRepo}
}
func (productServer) mustEmbedUnimplementedProductServer() {}

// implement method GetProducts from interface ProductServer
func (s productServer) GetProducts(*Nothing, Product_GetProductsServer) error {
	products, err := s.prodRepo.GetAll()
	if err != nil {
		return err
	}
	prodResponses := []*ProductResponse{}

	for _, product := range products {
		prodString := ProductResponse{
			ID:          uint32(product.ID),
			ProductName: product.Product_name,
			Price:       float32(product.Price),
			DateCreated: product.Date_created,
		}
		prodResponses = append(prodResponses, &prodString)
	}
	logs.Info("GetProducts", zap.Any("customers", prodResponses))
	return nil
}
func (s productServer) GetProduct(ctx context.Context, req *IDRequest) (*ProductResponse, error) {
	product, err := s.prodRepo.GetById(int(req.ID))
	if err != nil {
		return nil, err
	}
	prodString := ProductResponse{
		ID:          uint32(product.ID),
		ProductName: product.Product_name,
		Price:       float32(product.Price),
		DateCreated: product.Date_created,
	}
	return &prodString, nil
}
func (s productServer) InsertProduct(ctx context.Context, req *ProductRequest) (*ProductResponse, error) {
	product := repository.Product{
		Product_name: req.ProductName,
		Price:        float64(req.Price),
	}
	id, err := s.prodRepo.InsertProduct(product)
	if err != nil {
		return nil, err
	}
	return &ProductResponse{ID: uint32(id)}, nil
}
func (s productServer) DeleteProduct(ctx context.Context, req *IDRequest) (*ProductResponse, error) {
	err := s.prodRepo.DeleteProduct(int(req.ID))
	if err != nil {
		return nil, err
	}
	return &ProductResponse{ID: req.ID}, nil
}
func (s productServer) UpdateProduct(ctx context.Context, req *ProductRequest) (*ProductResponse, error) {
	product := repository.Product{
		ID:           int(req.ID),
		Product_name: req.ProductName,
		Price:        float64(req.Price),
	}
	id, err := s.prodRepo.UpdateProduct(product)
	if err != nil {
		return nil, err
	}
	return &ProductResponse{ID: uint32(id)}, nil
}

// func (s productService) GetProducts() ([]ProductResponse, error) {
// 	products, err := s.prodRepo.GetAll()
// 	if err != nil {
// 		logs.Error(err)
// 		return nil, errs.NewUnexpectedError()
// 	}
// 	prodResponses := []ProductResponse{}

// 	for _, product := range products {
// 		prodString := fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s",
// 			product.ID, product.Product_name, product.Price, product.Date_created)
// 		prodStrings = append(prodStrings, prodString)
// 		prodResponse := ProductResponse{
// 			ID:          product.ID,
// 			ProductName: product.Product_name,
// 			Price:       product.Price,
// 			DateCreated: product.Date_created,
// 		}
// 		prodResponses = append(prodResponses, prodResponse)
// 	}
// 	responseString := strings.Join(prodStrings, "\n")
// 	logs.Info("Get datas ===>" + responseString)
// 	return prodResponses, nil
// }
// func (s productService) GetProduct(id int) (*ProductResponse, error) {
// 	product, err := s.prodRepo.GetById(id)
// 	if err != nil {
// 		if err == sql.ErrNoRows {
// 			return nil, errs.NewNotFoundError("product not found")
// 		}
// 		logs.Error(err)
// 		return nil, errs.NewUnexpectedError()
// 	}
// 	prodResponse := ProductResponse{
// 		ID:          product.ID,
// 		ProductName: product.Product_name,
// 		Price:       product.Price,
// 		DateCreated: product.Date_created,
// 	}
// 	logs.Info("Get data ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
// 	return &prodResponse, nil
// }
// func (s productService) DeleteProduct(id int) error {
// 	product, _ := s.prodRepo.GetById(id)
// 	err := s.prodRepo.DeleteProduct(id)
// 	if err != nil {
// 		logs.Error(err)
// 		return errs.NewNotFoundError("Product Not Found")
// 	}

// 	logs.Info("Delete data ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
// 	return nil
// }

// func (s productService) InsertProduct(ProductResponse ProductResponse) (int, error) {
// 	Product := repository.Product{
// 		ID:           ProductResponse.ID,
// 		Product_name: ProductResponse.ProductName,
// 		Price:        ProductResponse.Price,
// 		Date_created: ProductResponse.DateCreated,
// 	}

// 	ProductID, err := s.prodRepo.InsertProduct(Product)
// 	product, _ := s.prodRepo.GetById(ProductID)
// 	if err != nil {
// 		logs.Error(err)
// 		return 0, errs.NewValidationError("Validation Error")
// 	}
// 	logs.Info("Get data ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
// 	return ProductID, nil

// }

// func (s productService) UpdateProduct(ProductResponse ProductResponse) (int, error) {
// 	Product := repository.Product{
// 		ID:           ProductResponse.ID,
// 		Product_name: ProductResponse.ProductName,
// 		Price:        ProductResponse.Price,
// 		Date_created: ProductResponse.DateCreated,
// 	}

// 	ProductID, err := s.prodRepo.UpdateProduct(Product)
// 	product, _ := s.prodRepo.GetById(ProductID)

// 	if err != nil {
// 		logs.Error(err)
// 		return 0, errs.NewValidationError("Validation Error")
// 	}
// 	logs.Info("Update Product ===>" + fmt.Sprintf("ID: %d, ProductName: %s, Price: %f, DateCreated: %s", product.ID, product.Product_name, product.Price, product.Date_created))
// 	return ProductID, nil
// }
