package services

import (
	"context"
	"fmt"
)

type ProductService interface {
	GetProducts() error
	GetProduct(id int32) error
	InsertProduct(name string, price float32) error
	DeleteProduct(id int32) error
	UpdateProduct(id int32, name string, price float32) error
}
type productService struct {
	productService ProductClient
}

func NewProductService(productClient ProductClient) ProductService {
	return productService{productClient}
}
func (base productService) GetProducts() error {
	req := Nothing{}
	res, err := base.productService.GetProducts(context.Background(), &req)
	fmt.Printf("Service : GetProducts\n")
	if err != nil {
		return err
	}
	fmt.Printf("Request : %v\n", "Get all of product")
	fmt.Printf("Response: %v\n", res)
	return nil
}
func (base productService) GetProduct(id int32) error {
	req := IDRequest{ID: uint32(id)}
	res, err := base.productService.GetProduct(context.Background(), &req)
	if err != nil {
		return err
	}
	fmt.Printf("Service : GetProduct\n")
	fmt.Printf("Request : %v\n", req.String())
	fmt.Printf("Response: %v\n", res)
	return nil
}
func (base productService) InsertProduct(name string, price float32) error {
	req := ProductRequest{ProductName: name, Price: price}
	res, err := base.productService.InsertProduct(context.Background(), &req)
	if err != nil {
		return err
	}
	fmt.Printf("Service : InsertProduct\n")
	fmt.Printf("Request : %v\n", req.String())
	fmt.Printf("Response: %v\n", res.ID)
	return nil
}
func (base productService) DeleteProduct(id int32) error {
	req := IDRequest{ID: uint32(id)}
	res, err := base.productService.DeleteProduct(context.Background(), &req)
	if err != nil {
		return err
	}
	fmt.Printf("Service : DeleteProduct\n")
	fmt.Printf("Request : %v\n", req.ID)
	fmt.Printf("Response: %v\n", res)
	return nil
}
func (base productService) UpdateProduct(id int32, name string, price float32) error {
	req := ProductRequest{ID: uint32(id), ProductName: name, Price: price}
	res, err := base.productService.UpdateProduct(context.Background(), &req)
	if err != nil {
		return err
	}
	fmt.Printf("Service : UpdateProduct\n")
	fmt.Printf("Request : %v\n", req.ID)
	fmt.Printf("Response: %v\n", res)
	return nil
}
